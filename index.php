<?php

require 'Janitor/Janitor.php';
try {
	(new Janitor())->run(new JanitorConfDefault());
}catch (Exception $e){
	LOG_DEBUG($e->getMessage());
	exit();
}
