<?php

class JanitorContext{
	
	/**
	 * 
	 * @var JanitorCacheCidr
	 */
	public $ipDict;
	/**
	 * 
	 * @var JanitorCacheCustomer
	 */
	public $userDict;
	
	/**
	 * 
	 * @var array
	 */
	public $pluginCaches = array();
	
	/**
	 * 
	 * @var JanitorConfDefault
	 */
	public $janitorConf;
	/**
	 * 
	 * @var JanitorFilterManager
	 */
	public $filterInstance;
	
	/**
	 * 
	 * @var swoole_atomic
	 */
	public $requestNums;
	
	/**
	 * 
	 * @var JanitorAdminSqlpool
	 */
	public $sqlPool;
	
	
	public $stats = array("connection"=>0);
	
	
	public function __construct(){
		$this->requestNums = new swoole_atomic();
		$this->userDict = new JanitorCacheCustomer('janitor_customer', 1024*10);
		$this->ipDict = new JanitorCacheCidr('janitor_ip_filter', 1024*5);
		
		$root = JanitorConfiguration::getProperty("janitor.filter.root");
		$filtersDirectory = array(
			dirname(__FILE__).DIRECTORY_SEPARATOR.$root.DIRECTORY_SEPARATOR,
		);
		JanitorFilterManager::getInstance($this)->init($filtersDirectory);
		
		$this->filterInstance = JanitorFilterManager::getInstance($this);
	}
	
	public function incrRequestNum(){
		$this->requestNums->add(1);
	}
	
	/**
	 * 
	 * @param unknown $pluginName
	 * @param unknown $cacheName
	 * @throws JanitorException
	 * @return JanitorCacheBase
	 */
	public function getCache($name){
		if (isset($this->pluginCaches[$name])){
			return $this->pluginCaches[$name];
		}else{
			throw new JanitorException("can not found cache for name {$name}");
		}
	}
}