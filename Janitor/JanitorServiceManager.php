<?php
class JanitorServiceManager{
	
	protected $services = array();
	
	protected $janitorContext;
	
	public function __construct(JanitorContext $context){
		$this->janitorContext = $context;
	}
	
	public function addService(JanitorService $service){
		$this->services[] = $service;
	}
	
	public function startAllService(){
		foreach ($this->services as $service){
			if ($service instanceof JanitorService){
				$service->start($this->janitorContext);
			}
		}
		if ($this->janitorContext->janitorConf->getEventloop()){
			$this->janitorContext->janitorConf->getEventloop()->start();
		}
	}
	
	public function stopAllService(){
		foreach ($this->services as $service){
			if ($service instanceof JanitorService){
				$service->stop($this->janitorContext);
			}
		}
	}
}