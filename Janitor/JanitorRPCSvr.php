<?php

class JanitorRPCSvr implements JanitorService{

	protected $rpcSvrConf;
	
	protected $janitorConf;
	
	protected $implSvr;
	
	public function start(JanitorContext $context){
		$this->rpcSvrConf = $context->janitorConf->getRPCSvrConf();
		$this->janitorConf = $context->janitorConf;
		
		$eventloop = $this->janitorConf->getEventloop();
		
		if ($eventloop){
			$rpcSvr = $eventloop->addlistener($this->rpcSvrConf->rpcSvrHost, $this->rpcSvrConf->rpcSvrPort, SWOOLE_SOCK_UDP);
			$rpcSvr->set(array(
					'daemonize'=>true,
					'log_file'=>$this->rpcSvrConf->logFile,
					'pid_file'=>$this->rpcSvrConf->pidFile,
			));
			$this->implSvr = new JanitorRpcImplsvr($context);
			
			$rpcSvr->on('connect',array($this->implSvr,'onConnect'));
			$rpcSvr->on('close',array($this->implSvr,'onClose'));
			$rpcSvr->on('packet',array($this->implSvr,'onPacket'));
			
		}else{
			throw new Exception("can not init stat svr before api svr is inited...");
		}
	}

	public function stop(JanitorContext $context){
		var_dump('stop rpc svr');
	}
}