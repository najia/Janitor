<?php

class JanitorHttpContext{
	
	/**
	 * 
	 * @var Swoole\Coroutine\Http\Client
	 */
	public $upstream;
	/**
	 * 
	 * @var JanitorContext
	 */
	public $janitorCtx;
	
	/**
	 * 
	 * @var unknown
	 */
	public $userCtx;
	/**
	 * 
	 * @var swoole_http_request
	 */
	public $request;
	/**
	 * 
	 * @var swoole_http_response
	 */
	public $response;
	
	public $responseBody;
	
	public $responseHeaders;
	
	public $routingDebug;
	
	public $filterChainPos = array();
	
	public $extractor;
	
	public function __construct(swoole_http_request $request,swoole_http_response $response){
		$this->request = $request;
		$this->response = $response;
		$this->routingDebug = array();
		$this->extractor = new JanitorExtractor($request);
		
		$this->filterChainPos[JanitorFilterManager::FILTER_TYPE_PRE] = new swoole_atomic(0);
		$this->filterChainPos[JanitorFilterManager::FILTER_TYPE_ROUTE] = new swoole_atomic(0);
		$this->filterChainPos[JanitorFilterManager::FILTER_TYPE_POST] = new swoole_atomic(0);
	}
	
	public function debugRequest(){
		return true;
// 		return true || $this->extractor->header("debugRequest")||
// 				$this->extractor->query("debugRequest")||
// 				$this->extractor->query("debugRequest");
	}
	
	
	public function addJanitorHeader($key,$value){
		if (!empty($key)){
			$this->responseHeaders[$key] = $value;
			LOG_DEBUG("%s>>%s=%s",__METHOD__,$key,$value);
		}
		return $this;
	}
	
}