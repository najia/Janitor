<?php

class JanitorApiImplsvr{
	
	/**
	 * 
	 * @var JanitorContext
	 */
	protected $ctx;
	
	public function __construct(JanitorContext $context){
		$this->ctx = $context;
	}
	
	public function onWorkerStart(swoole_server $svr,$workerId){
		//预加载所有的filter
		
	}
	
	public function onRequest($request,swoole_http_response $response){
		$this->ctx->incrRequestNum();
		$httpCtx = new JanitorHttpContext($request, $response);
		$httpCtx->janitorCtx = $this->ctx;
		try {
			JanitorFilterManager::getInstance($this->ctx)->runFilter($httpCtx);
		} catch (JanitorException $e) {
			$response->status(500);
			$response->end($e->getMessage());
		}finally {
			unset($httpCtx);
		}
	}
}