<?php
/**
 * Class for janitor classloader.
 */

set_include_path(get_include_path().PATH_SEPARATOR.implode(PATH_SEPARATOR,array(
	dirname(dirname(__FILE__))
)));

class JanitorClassloader{

	public static function __autoload($classname){
		if (is_file(dirname(__FILE__).DIRECTORY_SEPARATOR.$classname.'.php')) {
			require_once dirname(__FILE__).DIRECTORY_SEPARATOR.$classname.'.php';
			return true;
		}
		$segments = array();
		$start = 0;
		$pointer = 1;
		while ($pointer<strlen($classname)) {
			$ch = $classname[$pointer];
			if ($ch>='A' && $ch <= 'Z') {
				$segments[] = substr($classname,$start,$pointer-$start);
				$start = $pointer;
			}
			if ($pointer+1==strlen($classname)) {
				$segments[] = substr($classname,$start);
			}
			$pointer++;
		}
		$filePath = implode(DIRECTORY_SEPARATOR,$segments);
		$includePath = explode(PATH_SEPARATOR, get_include_path());

		foreach ($includePath as $path) {
			if (is_file($path.DIRECTORY_SEPARATOR.$filePath.'.php')) {
				require_once $path.DIRECTORY_SEPARATOR.$filePath.'.php';
				return true;
			}

			if (is_file($path.DIRECTORY_SEPARATOR.$filePath.'.class.php')) {
				require_once $path.DIRECTORY_SEPARATOR.$filePath.'.class.php';
				return true;
			}
		}
		return false;		
	}
}

spl_autoload_register(array('JanitorClassloader','__autoload'));