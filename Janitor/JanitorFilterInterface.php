<?php

abstract class JanitorFilterInterface{
	
	const DYNAMIC_FILTER  = 1;
	const STATIC_FILTER = 2;
	
	public $config = array(
		'enable'=>true,
		'type'=>self::STATIC_FILTER,
		'name'=>'',
		'condition'=>array(),
		'extractor'=>array(),
		'action'=>array()
	);
	
	abstract public function getFilterName();
	
	abstract public function getFilterType();
	
	abstract public function getPriority();
	
	abstract public function shouldFilter(JanitorHttpContext $ctx);
	
	abstract public function run(JanitorHttpContext $ctx);
}