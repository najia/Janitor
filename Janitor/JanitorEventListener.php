<?php

class JanitorEvent{
	
	public $what;
	
	public $data;
	
	public function __construct($what,$data){
		$this->what = $what;
		$this->data = $data;
	}
}

interface JanitorEventListener{
	
	/**
	 * 时间发生
	 */
	public function onInvoke(JanitorContext $ctx,JanitorEvent $evt);
	
}