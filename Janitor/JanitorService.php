<?php

interface JanitorService{
	
	public function start(JanitorContext $context);

	public function stop(JanitorContext $context);
}