<?php

class JanitorAdminSqlpool{
	
	protected $maxPoolSize;
	
	protected $minPoolSize;
	
	protected $startPoolSize;
	
	protected $currentConNum;
	
	protected $freePool = array();
	
	protected $busyPool = array();
	
	protected $host;
	
	protected $username;
	
	protected $password;
	
	protected $database;
	
	protected $lock;
	
	public function __construct(JanitorContext $ctx){
		
		$this->maxPoolSize = JanitorConfiguration::getInt("janitor.database.max_pool_size",20);
		$this->minPoolSize= JanitorConfiguration::getInt("janitor.database.min_pool_size",5);
		$this->startPoolSize = JanitorConfiguration::getInt("janitor.database.start_pool_size",10);
		
		$this->host = JanitorConfiguration::getProperty('janitor.database.host');
		$this->username = JanitorConfiguration::getProperty("janitor.database.username");
		$this->password = JanitorConfiguration::getProperty("janitor.database.password");
		$this->database = JanitorConfiguration::getProperty("janitor.database.dbname");
		$this->lock = new swoole_lock();
		$this->currentConNum = new swoole_atomic(0);
		$this->initPool();
	}
	
	protected function initPool(){
		LOG_DEBUG("try to allocate database pool with maxPoolSize=%d,minPoolSize=%d,startPoolSize=%d",$this->maxPoolSize,$this->minPoolSize,$this->startPoolSize);
		for ($i=0;$i<$this->startPoolSize;$i++){
			$clients = mysqli_connect($this->host,$this->username,$this->password,$this->database);
			if (!$clients){
				throw new JanitorException("can not init the sql pool:".mysqli_connect_error());
			}
			$status = mysqli_query($clients, "set names utf8");
			LOG_DEBUG("success allocate database pool with link:%s,with query set names utf8=%d",mysqli_thread_id($clients),$status);
			$this->freePool[] = $clients;
		}
	}
	
	public function getConnection(){
		$this->lock->lock();
		if (count($this->freePool)<=0){
			if ($this->currentConNum>=$this->maxPoolSize){
				throw new JanitorException("exceed the sql pool max",-1);
			}
			$clients = mysqli_connect($this->host,$this->username,$this->password,$this->database);
			if (!$clients){
				throw new JanitorException("can not init the sql pool:".mysqli_connect_error());
			}
			$status = mysqli_query($clients, "set names utf8");
			LOG_DEBUG(">>>success reallocate database pool with link:%s,with query set names utf8=%d",mysqli_thread_id($clients),$status);
			$this->freePool[] = $clients;
		}
		$freeCon = array_shift($this->freePool);
		$this->busyPool[mysqli_thread_id($freeCon)] = $freeCon;
		$this->lock->unlock();
		LOG_DEBUG(">>>get connection:%s from pool",mysqli_thread_id($freeCon));
		return $freeCon;
	}
	
	public function info(){
		$data = array();
		foreach ($this->freePool as $k=>$con) {
			$data[mysqli_thread_id($con)] = 1;
		}
		foreach ($this->busyPool as $k=>$con) {
			if (!is_null($con))
				$data[mysqli_thread_id($con)] = 1;
		}
		return array('list'=>$data,'numFree'=>count($this->freePool),'numBusy'=>count($this->busyPool));
	}
	
	public function returnConnection($con){
		$this->lock->lock();
		LOG_DEBUG(">>>return connection:%s to pool",mysqli_thread_id($con));
		foreach ($this->busyPool as $k=>$bcon){
			if ($k==mysqli_thread_id($con) && !is_null($bcon)){
				$this->freePool[] = $con;
				$this->busyPool[$k] = null;
			}
		}
		$this->lock->unlock();
		return true;
	}
}