<?php

class UserModel{
	
	protected $redis;
	
	public function __construct(){
		$this->redis = new Swoole\Coroutine\Redis();
		$this->redis->connect('127.0.0.1', 6379);
	}
	
	public function addUser($params){
		
		LOG_DEBUG("connect to redis at:%s",$this->redis->keys("user"));
		return $this->redis->set($params['username'],json_encode($params));
	}
	
	public function getlist(){
		return $this->redis->get('yusaint');
	}
}