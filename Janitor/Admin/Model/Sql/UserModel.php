<?php

class UserModel{
	
	protected $handler;
	
	public function __construct(SQLHandler $handler){
		$this->handler = $handler;
	}
		
	public function addUser($params){
		ValidatorUtil::checkEmptyString($params['username']);
		ValidatorUtil::checkEmptyString($params['description']);
		$uid = StringUtil::guid();
		$sql = "insert into janitor_user(id,uid,username,description,add_time,status) values(NULL,'{$uid}','{$params["username"]}','{$params["description"]}',NOW(),0)";
		$id = $this->handler->insert($sql);
		return $uid;
	}
	
	public function getlist(){
		$sql = "select * from janitor_user";
		return $this->handler->query($sql);
	}
}