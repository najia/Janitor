<?php
define('STATUS_OK', 0);
define('STATUS_SUSPEND', 1);

define('RESPONSE_OK', 0);
define('RESPONSE_ERR', -1);
define('ERR_REDIRECT', 2);
define('ERR_SYS', 9000000);
define('ERR_NO_PERMIT', 900000001);
define('ERR_SERVICE_UNKNOW', 90000002);
define('ERR_INVALID_PARAM', 100000001);
define('ERR_DUP_USER', 1000001);
define('ERR_NON_EXISTS_USER', 1000002);

define('ERR_DY_IMPORT_FILE_NOT_EXISTS',20000001);

class MessageHelper{
	
	static $message = array(
			ERR_SYS					=>'系统错误',
			ERR_SERVICE_UNKNOW		=>'服务不存在',
			ERR_NO_PERMIT           =>'无权限登陆当前系统',
			ERR_INVALID_PARAM		=>'参数错误',
			ERR_DUP_USER			=>'很抱歉，该用户名已经存在！',
			ERR_NON_EXISTS_USER		=>'很抱歉，当前用户不存在!',
			ERR_INVALID_PWD			=>'很抱歉，您当前的密码不符合要求,请输入至少包含数字和字母的密码，长度不少于6位!',
			
			ERR_DY_IMPORT_FILE_NOT_EXISTS=>'选择的文件不存在！',
			
	);
	
	public static function getMessage($code){
		return isset(self::$message[$code])?self::$message[$code]:self::$message[ERR_UNKONW];
	}
}
