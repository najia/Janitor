<?php

class AdminClassloader{
	
	public static function __autoload($classname){
		if (is_file(dirname(__FILE__).DIRECTORY_SEPARATOR.$classname.'.php')) {
			require_once dirname(__FILE__).DIRECTORY_SEPARATOR.$classname.'.php';
			return true;
		}
		if (is_file(dirname(__FILE__).DIRECTORY_SEPARATOR.'Util'.DIRECTORY_SEPARATOR.$classname.'.php')){
			require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'Util'.DIRECTORY_SEPARATOR.$classname.'.php';
			return true;
		}
		$segments = explode('_', $classname);
		$fileName = '';
		foreach ($segments as $segment){
			$fileName .= ucfirst($segment);
		}
		$fileName .='.php';
		unset($segments[0]);
		$segments = array_reverse($segments);
		
		$classFile = sprintf('%s%s%s',implode(DIRECTORY_SEPARATOR, $segments),DIRECTORY_SEPARATOR,$fileName);
		$path = dirname(__FILE__).DIRECTORY_SEPARATOR;
		if (is_file($path.$classFile)){
			require_once $path.$classFile;
			return true;
		}
		return false;
	}
}

spl_autoload_register(array('AdminClassloader','__autoload'));