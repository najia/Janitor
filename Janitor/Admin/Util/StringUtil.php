<?php
class StringUtil{
	
	public static function isValidMobile($mobile){
		if (preg_match('/^[0]?1[3|5|8][0-9]{9}$/', $mobile)){
			return true;
		}else{
			return false;
		}
	}
	
	public static function isValidEmail($email){
		$pattern = '/(\w)+(\.\w)*@(\w)+(\.\w)+/ig';
		if (preg_match($pattern, $email)){
			return true;
		}else{
			return false;
		}
	}
	
	public static function createGUID($prefix=""){
		$str = md5(uniqid(mt_rand(), true));
		$uuid  = substr($str,0,8) . '-';
		$uuid .= substr($str,8,4) . '-';
		$uuid .= substr($str,12,4) . '-';
		$uuid .= substr($str,16,4) . '-';
		$uuid .= substr($str,20,12);
		return $prefix . $uuid;
	}
	
	/**
	 * 验证座机
	 * [023]-12345678
	 * @param unknown $phone
	 */
	public static function isValidPhone($phone){
		$pattern = '/[0-9]{3,4}-[0-9]{7,8}/i';
		if (preg_match($pattern, $phone)){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * [isChinese description]
	 * @param  [type]  $word [description]
	 * @return boolean       [description]
	 */
	public static function isChinese($word){
		$pattern = '^[\x7f-\xff]+$/';
		if (preg_match($pattern, $word)) {
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 *
	 */
	public static function isFromMobile(){
		// 如果有HTTP_X_WAP_PROFILE则一定是移动设备
		if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
		{
			return true;
		}
		// 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
		if (isset ($_SERVER['HTTP_VIA']))
		{
			// 找不到为flase,否则为true
			return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
		}
		// 脑残法，判断手机发送的客户端标志,兼容性有待提高
		if (isset ($_SERVER['HTTP_USER_AGENT']))
		{
			$clientkeywords = array ('nokia',
					'sony',
					'ericsson',
					'mot',
					'samsung',
					'htc',
					'sgh',
					'lg',
					'sharp',
					'sie-',
					'philips',
					'panasonic',
					'alcatel',
					'lenovo',
					'iphone',
					'ipod',
					'blackberry',
					'meizu',
					'android',
					'netfront',
					'symbian',
					'ucweb',
					'windowsce',
					'palm',
					'operamini',
					'operamobi',
					'openwave',
					'nexusone',
					'cldc',
					'midp',
					'wap',
					'mobile'
			);
			// 从HTTP_USER_AGENT中查找手机浏览器的关键字
			if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))){
				return true;
			}
		}
		// 协议法，因为有可能不准确，放到最后判断
		if (isset ($_SERVER['HTTP_ACCEPT'])){
			// 如果只支持wml并且不支持html那一定是移动设备
			// 如果支持wml和html但是wml在html之前则是移动设备
			if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 *
	 * @param string $pureDigital
	 * @param string $withAlpha
	 */
	public static function randomString($len,$pureDigital = true, $withAlpha = false,$specialChar = false){
		$digital = '0123456789';
		$alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$special = '<>?\;!@#$%^&*()';
		$s = '';
		if ($pureDigital){
			$s .= $digital;
		}
		if ($withAlpha){
			$s .= $alpha;
		}
		if ($specialChar){
			$s .= $special;
		}
		$slen = strlen($s)-1;
		$random = '';
		for ($i = 0 ;$i<$len;$i++){
			$random .= $s[mt_rand(0, $slen)];
		}
		return $random;
	}
	
	public static function guid(){
		$guid = implode('-', array(StringUtil::randomString(6,true,true),StringUtil::randomString(6,true,true),StringUtil::randomString(6,true,true),StringUtil::randomString(6,true,true)));
		return $guid;
	}
	
	public static function jsonDecode($str){
		if (empty($str)){
			return false;
		}else{
			$ret = json_decode($str,true);
			if (function_exists('json_last_error')){
				if (json_last_error()){
					throw new CurlMalformedException(
							MessageHelper::getMessage(ERR_SYS),
							ERR_SYS
							);
				}
			}
		}
		return $ret;
	}
	
	public static function formatMongoTime($time){
		return date('Y-m-d H:i:s',$time->sec);
	}
	
	public static function gaussianNum($min,$max){
		$U1 = rand(0,10000)/10000;
		$U2 = rand(0,10000)/10000;
		$theta = 2*3.141592653*$U1;
		$R = sqrt(-2 * log($U2));
		$ret = $R*sin($theta)+($min+$max)/2;
		if ($ret<$min) {
			$ret = $min;
		}
		if ($ret>$max) {
			$ret = $max;
		}
		if ($ret<0) {
			$ret = 0;
		}
		return sprintf('%0.2f',$ret);
	}
	
	public static function getRealIp(){
		if (isset($_SERVER['HTTP_X_FORWARD_FOR'])){
			$ip = $_SERVER['HTTP_X_FORWARD_FOR'];
		}elseif (isset($_SERVER['HTTP_CLIENT_IP'])){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	public static function traceId($prefix = ''){
		$str = $prefix;
		$str .= substr(microtime(true),4,-1).uniqid().rand(0, 99999);
		return md5($str).self::randomString(10,true,true);
	}
	
	public static function MysqlEscapeString($str){
		return mysqli_escape_string($str);
	}
	
	public static function getFirstDayOfThisMonth(){
		return strtotime(date('Ym01',time()));
	}
	
	public static function getLastDayOfThisMonth(){
		return strtotime(date('Ym01',time()).' +1 month -1 day');
	}
	
	public static function maxDate(array $array){
		if(!empty($array) && is_array($array)){
			$maxDate = $array[0]['startTime'];
			foreach ($array as $item){
				if($maxDate < strtotime($item['startTime'])){
					$maxDate = strtotime($item['startTime']);
				}
			}
			return $maxDate;
		}
	}
	
	public static function sendMsg($phone, $content){
		SmsApi::getInstance()->sendSms($phone, $content);
	}
	
	public static function formatParamters($params, $urlencode){
		$buff = "";
		ksort($params);
		foreach ($params as $k => $v){
			if (null != $v && "null" != $v && "sign" != $k) {
				if($urlencode){
					$v = urlencode($v);
				}
				$buff .= $k . "=" . $v . "&";
			}
		}
		$reqPar;
		if (strlen($buff) > 0) {
			$reqPar = substr($buff, 0, strlen($buff)-1);
		}
		return $reqPar;
	}
	
	public static function arrayToXml($arr){
		$xml = "<xml>";
		foreach ($arr as $key=>$val)
		{
			if (is_numeric($val))
			{
				$xml.="<".$key.">".$val."</".$key.">";
				
			}
			else{
				$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
			}
		}
		$xml.="</xml>";
		return $xml;
	}
	
	public static function number_cut($k){
		$arr = explode(".",$k);
		if(empty($arr[1]))
			return strval($k);
			$a = substr($arr[1],0,2);
			$number = empty($a) ? '00' :  $a;
			$ok = $arr[0].'.'.$number;
			return strval($ok);
	}
	
}
