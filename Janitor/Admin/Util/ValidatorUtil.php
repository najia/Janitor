<?php
class ValidatorUtil{
	
	public static function checkEmptyString($str){
		if (empty($str)&&strlen($str)==0){
			throw new JanitorException(
					MessageHelper::getMessage(ERR_INVALID_PARAM),
					ERR_INVALID_PARAM
					);
		}
	}
	
	public static function checkEqual($test,$value,$errno = null){
		$errno = isset($errno)?$errno:ERR_INVALID_PARAM;
		if ($test!=$value){
			throw new JanitorException(
					MessageHelper::getMessage($errno),
					$errno
					);
		}
	}
	
	public static function checkPositiveInteger($int){
		if ($int<=0){
			throw new JanitorException(
					MessageHelper::getMessage(ERR_INVALID_PARAM),
					ERR_INVALID_PARAM
					);
		}
	}
	
	public static function checkGtZero($int){
		if ($int<0){
			throw new JanitorException(
					MessageHelper::getMessage(ERR_INVALID_PARAM),
					ERR_INVALID_PARAM
					);
		}
	}
	
	public static function checkEmptyArray($arr,$errno = null){
		$code = isset($errno)?$errno:ERR_INVALID_PARAM;
		if (!is_array($arr) || empty($arr)){
			throw new JanitorException(
					MessageHelper::getMessage($code),
					$code
					);
		}
	}
	
	public static function checkNull($arr){
		if (is_null($arr) || !isset($arr)){
			throw new InvalidArgumentException(
					MessageHelper::getMessage(ERR_INVALID_PARAM),
					ERR_INVALID_PARAM
					);
		}
	}
	
	/**
	 *
	 * @param unknown $v
	 * @param unknown $range
	 */
	public static function checkRange($v,$range){
		if (!is_array($range) || !in_array($v, $range)){
			throw new InvalidArgumentException(
					MessageHelper::getMessage(ERR_INVALID_PARAM),
					ERR_INVALID_PARAM
					);
		}
	}
}