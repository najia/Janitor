<?php

class StatController extends JanitorAdminControllerAbstract{
	
	public function index(){
		$ret = array('status'=>0,'data'=>'hello','msg'=>'');
		$this->httpResponse->end(json_encode($ret));
	}
	
	public function status(){
		try {
			$eventloop = $this->ctx->janitorCtx->janitorConf->getEventloop();
			$status = $eventloop->stats();
			$stats = $this->ctx->janitorCtx->stats;
			$poolInfo = $this->ctx->janitorCtx->sqlPool->info();
			$this->success(array_merge($stats,$status,$poolInfo));
		} catch (Exception $e) {
			$this->error($e);
		}
	}
}