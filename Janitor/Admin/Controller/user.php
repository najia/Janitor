<?php
class UserController extends JanitorAdminControllerAbstract{
	
	public function add(){
		try {
			$params = array(
				'username'=>$this->httpRequest->get('username'),
				'description'=>$this->httpRequest->get('description')
			);
			$this->success(Admin_User_Service::getInstance($this->ctx->janitorCtx)->addUser($params));
		} catch (Exception $e) {
			$this->error($e);
		}
	}
	
	public function getlist(){
		try {
			$this->success(Admin_User_Service::getInstance($this->ctx->janitorCtx)->getlist());
		} catch (Exception $e) {
			$this->error($e);
		}
	}
	
	public function getById(){
		try {
			$this->success(Admin_User_Service::getInstance($this->ctx->janitorCtx)->getById($this->httpRequest->request('uid')));
		} catch (Exception $e) {
			$this->error($e);
		}
	}
	
	public function delete(){
		try {
			$this->success(Admin_User_Service::getInstance($this->ctx->janitorCtx)->delete($this->httpRequest->get('uid')));
		} catch (Exception $e) {
			$this->error($e);
		}
	}
	
}