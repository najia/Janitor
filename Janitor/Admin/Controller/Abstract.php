<?php

class JanitorAdminHttpRequest{
	
	protected $httpRequest;
	
	public function __construct(swoole_http_request $request){
		$this->httpRequest = $request;
	}
	
	/**
	 *
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function request($key, $value = null) {
		$request = $this->httpRequest->get;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return $value;
	}
	public function getInt($key, $value = null) {
		$request = $this->httpRequest->get;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return intval ( $value );
	}
	public function getFloat($key, $value = null) {
		$request = $this->httpRequest->get;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return floatval ( $value );
	}
	
	/**
	 *
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function get($key, $value = null) {
		$request = $this->httpRequest->get;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return $value;
	}
	
	/**
	 *
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function post($key, $value = null) {
		$request = $this->httpRequest->post;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return $value;
	}
	
	/**
	 *
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function cookie($key, $value = null) {
		$request = $this->httpRequest->cookie;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return $value;
	}
	
	public function header($key, $value = null) {
		$request = $this->httpRequest->header;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return $value;
	}
	
	public function server($key, $value = null) {
		$request = $this->httpRequest->server;
		if (isset ( $request [$key] ))
			return $request[$key];
			else
				return $value;
	}
}

class JanitorAdminControllerAbstract{
	
	protected $ctx;
	/**
	 * 
	 * @var JanitorAdminHttpRequest
	 */
	protected $httpRequest;
	
	protected $httpResponse;
	
	public function __construct(JanitorAdminContext $ctx){
		$this->ctx = $ctx;
		$this->httpRequest = new JanitorAdminHttpRequest($this->ctx->request);
		$this->httpResponse = $this->ctx->response;
	}
	
	
	protected function success($data){
		$ret = array(
			'status'=>0,
			'data'=>$data,
			'msg'=>'',
		);
		$callback = $this->httpRequest->get('callback');
		$this->httpResponse->end($callback.'('.json_encode($ret).')');
	}
	
	protected function error(Exception $e){
		$ret = array(
				'status'=>$e->getCode(),
				'data'=>'',
				'msg'=>$e->getMessage(),
		);
		$callback = $this->httpRequest->get('callback');
		$this->httpResponse->end($callback.'('.json_encode($ret).')');
	}
}