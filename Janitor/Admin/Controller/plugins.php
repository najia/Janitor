<?php

class PluginsController extends JanitorAdminControllerAbstract{
	
	public function config(){
		try {
			$name = $this->httpRequest->get('name');//plugin name
			$this->success(Admin_Plugins_Service::getInstance($this->ctx->janitorCtx)->invoke($name, $this->httpRequest));
		} catch (Exception $e) {
			$this->error($e);
		}
	}
}