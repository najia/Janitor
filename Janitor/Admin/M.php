<?php

class SQLHandler{
	
	protected $pool;
	
	public function __construct(JanitorAdminSqlpool $pool){
		$this->pool = $pool;
	}
	
	public function query($sql){
		try {
			$con = $this->pool->getConnection();
			$result = mysqli_query($con, $sql);
			$data = array();
			if ($result){
				while (($row=mysqli_fetch_assoc($result))){
					$data[] = $row;
				}
			}else{
				throw new Exception(mysqli_error($con),mysqli_errno($con));
			}
			return $data;
		} catch (Exception $e) {
			throw new JanitorException($e->getMessage(),$e->getCode());
		}finally {
			$this->pool->returnConnection($con);
		}
	}
	
	public function update($sql){
		try {
			$con = $this->pool->getConnection();
			$result = mysqli_query($con, $sql);
			$data = array();
			if ($result){
				return mysqli_affected_rows($con);
			}else{
				throw new Exception(mysqli_error($con),mysqli_errno($con));
			}
		} catch (Exception $e) {
			throw new JanitorException($e->getMessage(),$e->getCode());
		}finally {
			$this->pool->returnConnection($con);
		}
	}
	
	public function insert($sql){
		try {
			$con = $this->pool->getConnection();
			$result = mysqli_query($con, $sql);
			if ($result){
				return mysqli_insert_id($con);
			}else{
				throw new Exception(mysqli_error($con),mysqli_errno($con));
			}
		} catch (Exception $e) {
			throw new JanitorException($e->getMessage(),$e->getCode());
		}finally {
			$this->pool->returnConnection($con);
		}
	}
	
	public function delete($sql){
		try {
			$con = $this->pool->getConnection();
			$result = mysqli_query($con, $sql);
			$data = array();
			if ($result){
				return mysqli_affected_rows($con);
			}else{
				throw new Exception(mysqli_error($con),mysqli_errno($con));
			}
			return $data;
		} catch (Exception $e) {
			throw new JanitorException($e->getMessage(),$e->getCode());
		}finally {
			$this->pool->returnConnection($con);
		}
	}
}

class RedisHandler{
	
}

class M{
	
	public static $pool;
	
	public static function getInstance($modelName,$type='redis'){
		switch ($type){
			case 'redis':
				if (is_file(dirname(__FILE__).DIRECTORY_SEPARATOR.'Model/Redis'.DIRECTORY_SEPARATOR.ucfirst($modelName).'Model.php')){
					require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'Model/Redis'.DIRECTORY_SEPARATOR.ucfirst($modelName).'Model.php';
					$clsName = ucfirst($modelName).'Model';
					return new $clsName(new RedisHandler());
				}
				LOG_DEBUG(dirname(__FILE__).DIRECTORY_SEPARATOR.'Model/Redis'.DIRECTORY_SEPARATOR.ucfirst($modelName).'Model.php');
				break;
				
			case 'sql':
				if (is_file(dirname(__FILE__).DIRECTORY_SEPARATOR.'Model/Sql'.DIRECTORY_SEPARATOR.ucfirst($modelName).'Model.php')){
					require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'Model/Sql'.DIRECTORY_SEPARATOR.ucfirst($modelName).'Model.php';
					$clsName = ucfirst($modelName).'Model';
					return new $clsName(new SQLHandler(self::$pool));
				}
				break;
		}
	}
}