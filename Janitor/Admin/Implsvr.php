<?php

define('CONTROLLER', dirname(__FILE__).DIRECTORY_SEPARATOR.'Controller');
define("ADMIN_ROOT", dirname(__FILE__).DIRECTORY_SEPARATOR);
define('PLUGINS_ROOT', _JANITOR_ROOT.'Plugins'.DIRECTORY_SEPARATOR);
define('WEBAPP', dirname(__FILE__).DIRECTORY_SEPARATOR.'webapp'.DIRECTORY_SEPARATOR);

require_once ADMIN_ROOT. 'AdminClassloader.php';

class JanitorAdminImplsvr{
	
	/**
	 * 
	 * @var JanitorContext
	 */
	protected $ctx;
	
	public function __construct(JanitorContext $context){
		$this->ctx = $context;
		M::$pool = $context->sqlPool;
	}
	
	public function onRequest(swoole_http_request $request,swoole_http_response $response){
		$server = $request->server;
		$pathInfo = $server['path_info'];
		if (is_file(WEBAPP.$pathInfo)){
			if (preg_match('/\.css$/', $pathInfo)){
				$response->header('Content-Type', 'text/css');
			}
			if (preg_match('/\.js$/', $pathInfo)){
				$response->header('Content-Type', 'application/javascript');
			}
			$response->sendfile(WEBAPP.$pathInfo);
			return;
		}
		$adminCtx = new JanitorAdminContext($request, $response);
		$response->header('Content-Type', 'application/json');
		$adminCtx->janitorCtx = $this->ctx;
		try {
			$this->route($adminCtx);
		} catch (Exception $e) {
			$response->status($e->getCode());
			$ret = array('status'=>-1,'data'=>'','msg'=>$e->getMessage());
			$response->end(json_encode($ret));
		}
	}
	
	
	protected function route(JanitorAdminContext $adminCtx){
		$uri = $adminCtx->request->server['path_info'];
		$uri = trim($uri,'/');
		$uri = strtolower($uri);
		$controllerName = '';
		$actionName = '';
		if (empty($uri)){
			$uri = '/';
		}
		if ($uri=='/'){
			$controllerPath = CONTROLLER.DIRECTORY_SEPARATOR.'index.php';
			$controllerName = 'IndexController';
			$actionName = 'index';
		}else{
			// /p1/p2/controller/action
			$segments = explode('/', $uri);
			$path = CONTROLLER.DIRECTORY_SEPARATOR;
			foreach ($segments as $k=>$segment){
				if (is_file($path.$segment.'.php')){
					$controllerPath = $path.$segment.'.php';
					$controllerName = ucfirst($segment).'Controller';
					if ($k==count($segments)-1){
						$actionName = 'index';
					}else{
						$actionName = $segments[$k+1];
					}
					break;
				}else{
					$path = $path.$segment.DIRECTORY_SEPARATOR;
				}
			}
		}
		if (empty($controllerName) || empty($actionName)){
			throw new JanitorException("can not found api for {$uri}",404);
		}
		require_once $controllerPath;
		try {
			$reflection = new ReflectionClass($controllerName);
			$controller = $reflection->newInstanceArgs(array($adminCtx));
			$action = $reflection->getMethod($actionName);
			if (!$action->isPublic()){
				throw new JanitorException("can not found api for {$uri}",404);
			}
			if (isset($adminCtx->request->get))
				LOG_DEBUG("Janitor Admin Request Params==>".json_encode($adminCtx->request->get));
			$action->invoke($controller);
		} catch (Exception $e) {
			throw new JanitorException("can not found api for {$uri}",404);
		}
	}
}