<?php

class Base_Service{
	
	/**
	 * 
	 * @var JanitorContext
	 */
	protected $ctx;
	
	public static function getInstance(JanitorContext $ctx){
		static $m;
		if ($m==null){
			$m = new static();
			$m->ctx = $ctx;
		}
		return $m;
	}
}