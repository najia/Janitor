<?php

class Admin_User_Service extends Base_Service{
	
	protected $userNameDict;
	/**
	 * 
	 * @var UserModel
	 */
	protected $userModel;
	
	public function __construct(){
		$this->userModel = M::getInstance('user','sql');
	}
	
	public function addUser($params){
		$username = $params['username'];
		if (isset($this->userNameDict[$username])){
			throw new JanitorException("username={$username} is exists");
		}
		$uid = StringUtil::guid();
		$selector = $this->ctx->userDict->select();
		$user = $selector->where('uid', $uid)->commit();
		LOG_DEBUG(">>>>>>>>>>>".json_encode($user));
		if ($user){
			throw new JanitorException("uid={$uid} is exists");
		}else{
			$insertor = $this->ctx->userDict->insert();
			$insertor
				->set('uid', $uid)
				->set('name', $params['username'])
				->set('description', $params['description'])
				->set('add_time', time())
				->set('status', 0)
				->commit();
			$this->userNameDict[$username] = $uid;
		}
		return $selector->where('uid', $uid)->commit();
	}
	
	public function getlist(){
		return $this->ctx->userDict->select()->fetchAll();
	}
	
	public function getById($uid){
		$user = $this->ctx->userDict->select()->where('uid', $uid)->commit();
		if ($user){
			return $user;
		}else{
			return array();
		}
	}
	
	public function delete($uid){
		foreach ($this->ctx->userDict as $k=>$user){
			if ($user['uid']==$uid){
				unset($this->userNameDict[$user['username']]);
				unset($this->ctx->userDict[$k]);
				return true;
			}
		}
		throw new JanitorException("can not found uid=#{$uid}");
	}
}