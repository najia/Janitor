<?php

class Admin_Plugins_Service extends Base_Service{
	
	public function invoke($name,JanitorAdminHttpRequest $request){
		if (is_file(PLUGINS_ROOT.$name.DIRECTORY_SEPARATOR.'api.php')){
			require_once PLUGINS_ROOT.$name.DIRECTORY_SEPARATOR.'api.php';
			$segments = explode('-', $name);
			$className = '';
			foreach ($segments as $segment){
				$className .= ucfirst($segment);
			}
			try {
				$reflection = new ReflectionClass($className);
				$instance = $reflection->newInstance();
				if ($instance instanceof JanitorPluginsInterface){
					return $instance->config($this->ctx,$request);
				}
			} catch (Exception $e) {
				throw new JanitorException($e->getMessage()."can not found api for {$name}",404);
			}
		}
	}
}