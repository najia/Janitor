<?php

class JanitorLbImplsvr{
	
	/**
	 * 
	 * @var JanitorContext
	 */
	protected $ctx;
	
	public function __construct(JanitorContext $context){
		$this->ctx = $context;
	}
	
	public function onConnect(swoole_server $server, $fd, $from_id){
		var_dump("got connection from ".$from_id);
	}
	
	public function onClose(swoole_server $server, $fd, $reactorId){
		
	}
	
	public function onReceive(swoole_server $server, $fd, $reactor_id, $data){
		$server->send($fd, 'Swoole: '.$data."\n"."the current reqNum is:".$this->ctx->requestNums->get()."\n");
		$this->ctx->janitorConf->getEventloop()->reload();
	}
}