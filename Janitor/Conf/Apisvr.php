<?php


class JanitorConfApisvr{

	public $webSvrHost = '0.0.0.0';
	

	public $webSvrPort = 8001;
	
	public $parsePost = true;
	
	public $enableStaticHandler = true;
	
	
	public $logFile = 'api.logs';
	
	public $pidFile = __DIR__.DIRECTORY_SEPARATOR.'janitor.pid';

	/**
	 * API访问的基地址，例如http://localhost:8080/{baseUr}/user/info/get
	 *
	 * @var        string
	 */
	public $baseUrl = '';

	/**
	 * 默认的filter加载路径，可以是数组
	 *
	 * @var        string
	 */
	public $filterDirectory = 'Filters';
}