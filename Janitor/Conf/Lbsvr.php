<?php

class JanitorConfLbsvr{

	public $lbSvrHost = '0.0.0.0';
	
	public $lbSvrPort = 8003;
	
	public $logFile = 'loadbalance.logs';
	
	public $pidFile = __DIR__.DIRECTORY_SEPARATOR.'janitor.pid';
}