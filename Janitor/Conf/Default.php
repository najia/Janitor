<?php
/**
 * Class for janitor configuration default.
 */
class JanitorConfDefault{
	
	/**
	 * 
	 * @var swoole_http_server
	 */
	protected $eventloop;

	public function getAPIGatewayConf(){
		return new JanitorConfApisvr();
	}

	public function getAdminSvrConf(){
		return new JanitorConfAdminsvr();
	}

	public function getRPCSvrConf(){
		return new JanitorConfRpcsvr();
	}

	public function getLBSvrConf(){
		return new JanitorConfLbsvr();
	}

	

    /**
     * @return mixed
     */
    public function getEventloop()
    {
        return $this->eventloop;
    }

    /**
     * @param mixed $eventloop
     *
     * @return self
     */
    public function setEventloop($eventloop)
    {
        $this->eventloop = $eventloop;

        return $this;
    }
}