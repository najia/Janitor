<?php

class JanitorConfRpcsvr{
	
	public $rpcSvrHost = '0.0.0.0';
	
	public $rpcSvrPort = 8004;
	
	public $logFile = 'loadbalance.logs';
	
	public $pidFile = __DIR__.DIRECTORY_SEPARATOR.'janitor.pid';
}