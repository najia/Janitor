<?php

define('_JANITOR', 'Janitor');
define('_VERSION', '1.0.0');
define('_JANITOR_ROOT', dirname(__FILE__).DIRECTORY_SEPARATOR);

require_once 'JanitorClassloader.php';

/**
* 
*/
class Janitor{
	
	protected $serviceManager;

	public function __construct(){
		
	}

	/**
	 * { function_description }
	 *
	 * @param      JanitorConfigurationDefault  $janitorConf  The janitor conf
	 */
	public function run(JanitorConfDefault $janitorConf){
		try {
			
			
			JanitorConfiguration::init();
			
			JanitorLogger::setLoggerFile($janitorConf->getAPIGatewayConf()->logFile);

			$context = new JanitorContext();
			
			JanitorEventHandler::getInstance()->init($context);
			
			$context->janitorConf = $janitorConf;
			
			$this->serviceManager = new JanitorServiceManager($context);
			
			$this->serviceManager->addService(new JanitorApiSvr());
			$this->serviceManager->addService(new JanitorAdminSvr());
			$this->serviceManager->addService(new JanitorRPCSvr());
			$this->serviceManager->addService(new JanitorLBSvr());

			register_shutdown_function(array($this,'onShutdown'));
			
			$this->serviceManager->startAllService();
			
		} catch (Exception $e) {
			print_r($e);
		}
		
	}


	public function onShutdown(){
		try {
			$this->serviceManager->stopAllService();
		} catch (Exception $e) {
		}
	}
}



