<?php
/**
 * 储存urlmap的数据
 * name 映射的名字
 * host 需要处理的host名字，支持正则 *.api.com
 * uri  需要处理的uri 支持正则 /user/* 或者/user/api*
 * method 请求的方式 GET POST PUT DELETE
 * upstream_url 上游的请求url
 * retries 5
 * upstream_id
 * @author Administrator
 *
 */
class UrlProxyMapCache extends JanitorCacheBase {
	public function __construct($tableName, $size) {
		$this->definition = array (
				'proxy_id' => array (
						'primary' => true,
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'proxy_name' => array (
						'type' => self::TYPE_STRING,
						'size' => 64 
				),
				// 所属的部门id
				'host' => array (
						'type' => self::TYPE_STRING,
						'size' => 64 
				),
				// 所属的模块id
				'uri' => array (
						'type' => self::TYPE_STRING,
						'size' => 128 
				),
				// 当前状态
				'method' => array (
						'type' => self::TYPE_INT 
				),
				// upstream的权重，每一个backend可以对接多个upstream，分属多个机房，每一个upstream有多个host:port
				'upstream_url' => array (
						'type' => self::TYPE_STRING,
						'size' => 128 
				),
				// 健康度
				'retries' => array (
						'type' => self::TYPE_INT 
				),
				// 地址列表json
				'upstream_id' => array (
						'type' => self::TYPE_STRING,
						'size' => 64 
				) 
		);
		parent::__construct ( $tableName, $size );
	}
}