<?php
/**
 * 添加一个url->upstream的映射
 * 其中url可以是
 * @author Administrator
 *
 */
class UrlProxy implements JanitorPluginsInterface{
	
	const METHOD_GET = 1<<0;
	const METHOD_POST = 1<<1;
	const METHOD_PUT = 1<<2;
	const METHOD_DELETE = 1<<3;
	
	public function config(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$action = $request->get('action');
		switch ($action){
			case 'add':
				return $this->add($ctx, $request);
				break;
			case 'delete':
				return $this->delete($ctx, $request);
				break;
			case 'list':
				return $this->getlist($ctx,$request);
				break;
		}
		
	}
	
	protected function getlist(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$mapCache = $ctx->getCache(UrlProxyMapCache::getName());
		$list = array_values($mapCache->select()->fetchAll());
		foreach ($list as $k=>$v){
			$list[$k]['methods_list'] = array();
			if ($v['method']&self::METHOD_GET){
				$list[$k]['methods_list'][] = array('name'=>'GET');
			}
			if ($v['method']&self::METHOD_POST){
				$list[$k]['methods_list'][] = array('name'=>'POST');
			}
			if ($v['method']&self::METHOD_DELETE){
				$list[$k]['methods_list'][] = array('name'=>'DELETE');
			}
			if ($v['method']&self::METHOD_PUT){
				$list[$k]['methods_list'][] = array('name'=>'PUT');
			}
		}
		return $list;
	}
	
	/**
	 * 新增一个upstream
	 * @param JanitorContext $ctx
	 * @param JanitorAdminHttpRequest $request
	 */
	protected function add(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$mapCache = $ctx->getCache(UrlProxyMapCache::getName());
		$proxyName = $request->get('proxy_name');
		$host = $request->get('host');
		$uri = $request->get('uri');
		$methods = $request->get('methods');
		$upstreamUrl = $request->get('upstream_url');
		$retries = $request->get('retries');
		$upstreamId = $request->get('upstream_id');
		//解析host的正则，将 *.api.com 这种替换为  .+\.api\.com ,分两步，第一步将. 替换为\. 第二部将*替换为.+
		if (strpos($host, '*')!==false){
			$host = trim($host,'/');
			$host = str_replace('.', '\.', $host);
			$host = str_replace('*', '.+', $host).'$';
			$host = '/^'.$host.'/';
		}else{
			$host = trim($host,'/');
			$host = '/^'.$host.'$/';
		}
		if (strpos($uri, '*')!==false){
			$uri= str_replace('/', '\/', $uri);
			$uri= str_replace('.', '\.', $uri);
			$uri= str_replace('*', '.+', $uri).'$';
			$uri = '/^'.$uri.'$/';
		}else{
			$uri= str_replace('/', '\/', $uri);
			$uri = '/^'.$uri.'$/';
		}
		$targetMethod = 0;
		foreach ($methods as $method){
			$method = strtoupper($method);
			switch ($method){
				case 'GET':
					$targetMethod |= self::METHOD_GET;
					break;
				case 'POST':
					$targetMethod |= self::METHOD_POST;
					break;
				case 'PUT':
					$targetMethod |= self::METHOD_PUT;
					break;
				case 'DELETE':
					$targetMethod |= self::METHOD_DELETE;
					break;
			}
		}
		$upstreamUrl = trim($upstreamUrl);
		
		$upstreamCache = $ctx->getCache(LoadBalanceUpstreamCache::getName());
		$upstream = $upstreamCache->select()->findById($upstreamId);
		if (empty($upstream)){
			throw new JanitorException("can not found upstream by id=".$upstreamId,-1);
		}
		$mapCache
			->insert()
			->set('proxy_id', StringUtil::createGUID("proxy-"))
			->set('proxy_name', $proxyName)
			->set('host', $host)
			->set('uri', $uri)
			->set('method', $targetMethod)
			->set('upstream_url', $upstreamUrl)
			->set('retries', intval($retries))
			->set('upstream_id', $upstreamId)
			->commit();
		return $mapCache->select()->findById($proxyName);
	}
	
	protected function delete(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$proxyId = $request->get('proxy_id');
		ValidatorUtil::checkEmptyString($proxyId);
		$mapCache = $ctx->getCache(UrlProxyMapCache::getName());
		return $mapCache->delete()->where('proxy_id', $proxyId)->commit();
	}
}