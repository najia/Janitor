<?php
require_once 'api.php';
class UrlProxyFilter extends JanitorFilterInterface {
	public function __construct() {
	}
	public function getFilterName() {
		return __CLASS__;
	}
	public function getFilterType() {
		return 'pre';
	}
	public function getPriority() {
		return 1;
	}
	public function shouldFilter(JanitorHttpContext $ctx) {
		return true;
	}
	public function run(JanitorHttpContext $ctx) {
		$proxyCache = $ctx->janitorCtx->getCache ( UrlProxyMapCache::getName () );
		$httpRequest = $ctx->request;
		$requestMethod = strtoupper ( $httpRequest->server ['request_method'] );
		$requestURI = $httpRequest->server ['request_uri'];
		$host = $httpRequest->header ['host'];
		list ( $host, $port ) = explode ( ':', $host );
		
		$method = 0;
		switch ($requestMethod) {
			case 'GET' :
				$method = UrlProxy::METHOD_GET;
				break;
			case 'POST' :
				$method = UrlProxy::METHOD_POST;
				break;
			case 'PUT' :
				$method = UrlProxy::METHOD_PUT;
				break;
			case 'DELETE' :
				$method = UrlProxy::METHOD_DELETE;
				break;
		}
		$targetMatchRule = array ();
		$targetMatchTimes = 0;
		foreach ( $proxyCache->table as $proxy ) {
			$tmpMatchTimes = 0;
			$matchTimes = 0;
			if (! empty ( $proxy ['host'] )) {
				$matchTimes ++;
			}
			if (! empty ( $proxy ['uri'] )) {
				$matchTimes ++;
			}
			if ($proxy ['method'] > 0) {
				$matchTimes ++;
			}
			LOG_DEBUG ( ">>>>try to match %s", json_encode ( $proxy ) );
			if (! empty ( $proxy ['host'] ) && preg_match ( $proxy ['host'], $host )) {
				LOG_INFO ( "match host,pattern=%s,host=%s", $proxy ['host'], $host );
				$tmpMatchTimes ++;
			}
			if (! empty ( $proxy ['uri'] ) && preg_match ( $proxy ['uri'], $requestURI )) {
				LOG_INFO ( "match host,pattern=%s,uri=%s", $proxy ['uri'], $requestURI );
				$tmpMatchTimes ++;
			}
			if ($proxy ['method'] && ($method & $proxy ['method'])) {
				LOG_INFO ( "match host,pattern=%d,method=%s", $proxy ['method'], $requestMethod );
				$tmpMatchTimes ++;
			}
			if ($tmpMatchTimes >= $matchTimes && $tmpMatchTimes > $targetMatchTimes) {
				$targetMatchRule = $proxy;
				$targetMatchTimes = $tmpMatchTimes;
			}
		}
		if ($targetMatchTimes <= 0) {
			throw new JanitorException ( "can not found proxy for request" );
		}
		$upstreamCache = $ctx->janitorCtx->getCache ( LoadBalanceUpstreamCache::getName () );
		$upstreamId = $targetMatchRule ['upstream_id'];
		$upstream = $upstreamCache->select ()->findById ( $upstreamId );
		LOG_DEBUG ( "request info=%s", json_encode ( $httpRequest->header ) );
		LOG_DEBUG ( "find upstream info=%s", json_encode ( $upstream ) );
		
		$upstreamIPList = json_decode ( $upstream ['ip_list'] );
		
		shuffle ( $upstreamIPList );
		LOG_INFO ( "upstreamlist=%s", json_encode ( $upstreamIPList ) );
		foreach ( $upstreamIPList as $ip ) {
			$proxyHost = $ip->ip;
			$proxyPort = isset ( $ip->port ) ? $ip->port : '80';
			break;
		}
		LOG_INFO ( "proxy config found,proxy_ip=%s,proxy_port=%d", $proxyHost, $proxyPort );
		
		$cli = new Swoole\Coroutine\Http\Client ( $proxyHost, $proxyPort );
		$headers = $httpRequest->header;
		unset($headers['host']);
		array_walk($headers,function($k,$v){
			$k = ucfirst($k);
		});
		LOG_INFO("headers=%s",json_encode($headers));
		$cli->setHeaders ($headers);
		if (! empty ( $httpRequest->get )) {
			$cli->get ( $targetMatchRule ['upstream_url'] . '?' . http_build_query ( $httpRequest->get ) );
		} else {
			$cli->get ( $targetMatchRule ['upstream_url'] );
		}
		
		$ctx->upstream = $cli;
		LOG_INFO ( "request to %s:%d/%s", $proxyHost, $proxyPort, $targetMatchRule ['upstream_url'] . '?' . http_build_query ( $httpRequest->get ) );
		return true;
	}
}