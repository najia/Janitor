<?php

class IpRestrict implements JanitorPluginsInterface{
	
	public function config(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		LOG_DEBUG("got config data=".$request->request('ip'));
		
		$action = $request->get('action');
		switch ($action){
			case 'add':
				return $this->add($ctx, $request);
				break;
			case 'delete':
				return $this->delete($ctx, $request);
				break;
			case 'list':
				return $this->getlist($ctx,$request);
				break;
		}
		
	}
	
	protected function loadCidir(JanitorContext $ctx,$ipcidr){
		list($ip,$mask) = explode('/', $ipcidr);
		if ($mask<0 || $mask>32){
			throw new JanitorException("invalid cidr address");
		}
		$ipbin = ip2long($ip);
		LOG_DEBUG("ipbin=%x",$ipbin);
		$binMask = 0xFFFFFFFF<<intval(32-$mask);
		
		$binInvMask = $binMask^0xFFFFFFFF;
		
		LOG_DEBUG("mask=%s,inv_mask=%s",long2ip($binMask),long2ip($binInvMask));
		
		$low = $ipbin & $binMask;
		$higer = $low | $binInvMask;
		LOG_DEBUG("the cidir=%s,and lower ip=%s,higher ip=%s",$ipcidr,long2ip($low),long2ip($higer));
		$ctx->ipDict->insert()
		->set('ipcidr', $ipcidr)
		->set('lower_ip', $low)
		->set('upper_ip', $higer)
		->commit();
	}
	
	/**
	 * 加载cidir配置
	 * @param JanitorContext $ctx
	 */
	
	
	public function onInvoke(JanitorContext $ctx,JanitorEvent $evt){
		LOG_DEBUG("event %s happend,with data=%s",$evt->what,$evt->data);
		$this->loadCidir($ctx, $evt->data);
	}
	
	protected function getlist(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		return $ctx->ipDict->select()->fetchAll();
	}
	
	protected function add(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$ipcidr  = $request->request('ip');
		$this->loadCidir($ctx, $ipcidr);
		return $ctx->ipDict->select()->fetchAll();
	}
	
	protected function delete(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$ipcidr  = $request->request('ip');
		$ctx->ipDict->delete()
		->where('ipcidr', $ipcidr)
		->commit();
		return $ctx->ipDict->select()->fetchAll();
	}
}