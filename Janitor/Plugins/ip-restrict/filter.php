<?php

define('IP_RESTRICT_EVENT', 'onAddIPCidr');

class IpRestrictFilter extends JanitorFilterInterface{
	
	protected $isLoadCidir;
	
	public function __construct(){
		$this->isLoadCidir = false;
	}
	
	public function getFilterName(){
		return __CLASS__;
	}
	
	public function getFilterType(){
		return 'pre';
	}
	
	public function getPriority(){
		return 100;
	}
	
	public function shouldFilter(JanitorHttpContext $ctx){
		return true;
	}
	
	public function run(JanitorHttpContext $ctx){
		$ip = $ctx->extractor->server('remote_addr');
		$ipDict = $ctx->janitorCtx->ipDict;
		$ipbin = ip2long($ip);
		foreach ($ipDict->table as $cidr=>$info){
			if (($ipbin>=$info['lower_ip']) && ($ipbin<=$info['upper_ip'])){
				$ctx->response->status(403);
				$ctx->responseBody = "Your IP address is not allowed";
				return false;
			}
		}
	}
	
	
}