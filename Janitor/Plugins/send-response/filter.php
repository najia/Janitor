<?php

class SendResponseFilter extends JanitorFilterInterface{
	
	public function __construct(){
	}
	
	public function getFilterName(){
		return __CLASS__;
	}
	
	public function getFilterType(){
		return 'post';
	}
	
	public function getPriority(){
		return 1000;
	}
	
	public function shouldFilter(JanitorHttpContext $ctx){
		return true;//$ctx->response && $ctx->responseBody!=null;
	}
	
	public function run(JanitorHttpContext $ctx){
		$this->addResponseHeaders($ctx);
		$this->writeResponse($ctx);
	}
	
	protected function addResponseHeaders(JanitorHttpContext $ctx){
		$response = $ctx->response;
		$debugHeaders = '';
		$outputDebugHeader = $ctx->debugRequest();
		if ($outputDebugHeader){
			foreach ($ctx->routingDebug as $str){
				$debugHeaders .= "[{$str}]";
			}
		}
		if ($outputDebugHeader){
			$response->header("X-Janitor-Debug-Header", $debugHeaders);
		}
		LOG_DEBUG("%s=>enable debug requst:%d,resp=%s",__METHOD__,$ctx->debugRequest(),json_encode($ctx->responseHeaders));
		if ($ctx->debugRequest()){
			foreach ($ctx->responseHeaders as $key=>$value){
				$response->header($key, $value);
				LOG_DEBUG("OUTHEADER:<%s:%s>",$key,$value);
			}
		}else{
			foreach ($ctx->responseHeaders as $key=>$value){
				$response->header($key, $value);
			}
		}
	}
	
	protected function writeResponse(JanitorHttpContext $ctx){
		if (!$ctx->upstream){
			$ctx->response->write($ctx->responseBody);
		}else{
			$ctx->response->write($ctx->upstream->body);
			$ctx->upstream->close();
		}
		
	}
}