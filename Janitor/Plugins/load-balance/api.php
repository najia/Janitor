<?php

class LoadBalance implements JanitorPluginsInterface{
	
	public function config(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$action = $request->get('action');
		switch ($action){
			case 'add':
				return $this->add($ctx, $request);
				break;
			case 'delete':
				return $this->delete($ctx, $request);
				break;
			case 'list':
				return $this->getlist($ctx,$request);
				break;
		}
		
	}
	
	protected function getlist(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$upstreamCache = $ctx->getCache(LoadBalanceUpstreamCache::getName());
		$list = $upstreamCache->select()->fetchAll();
		foreach ($list as $k=>$row){
			$list[$k]['ip_list'] = json_decode($row['ip_list'],true);
		}
		return array_values($list);
	}
	
	/**
	 * 新增一个upstream
	 * @param JanitorContext $ctx
	 * @param JanitorAdminHttpRequest $request
	 */
	protected function add(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$upstreamCache = $ctx->getCache(LoadBalanceUpstreamCache::getName());
		$upstreamName = $request->get('upstream_name');//upstream名称
		$groupName = $request->get('group_name');//机房id
		$moduleName = $request->get('module_name');//模块id
		$weight = $request->get('weight',100);
		$ipList = $request->get('ip_list');
		ValidatorUtil::checkEmptyString($ipList);
		$ipListArray = json_decode($ipList,true);
		$hostInfo = array();
		foreach ($ipListArray as $ipInfo){
			if (strpos($ipInfo, '/')!=false){
				list($host,$hostWeight) = explode('/', $ipInfo);
			}else{
				$host = $ipInfo;
				$hostWeight = 100;
			}
			list($ip,$port) = explode(':', $host);
			$hostInfo[] = array('ip'=>$ip,'port'=>$port,'weight'=>$hostWeight,'status'=>0);
		}
		$upstreamCache->insert()
					->set('upstream_id', StringUtil::createGUID("upstream-"))
					->set('upstream_name', $upstreamName)
					->set('group_name', $groupName)
					->set('module_name', $moduleName)
					->set('status', 0)
					->set('weight', $weight)
					->set('health', 60)
					->set('ip_list', json_encode($hostInfo))
					->commit();
		return $upstreamCache->select()->fetchAll();
	}
	
	protected function delete(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$upstreamCache = $ctx->getCache(LoadBalanceUpstreamCache::getName());
		$upstreamId  = $request->request('upstream_id');
		$upstreamCache->delete()
		->where('upstream_id', $upstreamId)
		->commit();
		return $upstreamCache->select()->fetchAll();
	}
}