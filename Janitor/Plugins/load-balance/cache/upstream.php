<?php
/**
 * 储存所有的bucket
 * @author Administrator
 *
 */
class LoadBalanceUpstreamCache extends JanitorCacheBase {
	public function __construct($tableName, $size) {
		$this->definition = array (
				'upstream_id'=>array (
						'primary' => true,
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'upstream_name' => array (
						'type' => self::TYPE_STRING,
						'size' => 64 
				),
				// 所属的部门id
				'group_name' => array (
						'type' => self::TYPE_STRING,
						'size' => 64 
				),
				// 所属的模块id
				'module_name' => array (
						'type' => self::TYPE_STRING,
						'size' => 64 
				),
				// 当前状态
				'status' => array (
						'type' => self::TYPE_INT 
				),
				// upstream的权重，每一个backend可以对接多个upstream，分属多个机房，每一个upstream有多个host:port
				'weight' => array (
						'type' => self::TYPE_INT 
				),
				// 健康度
				'health' => array (
						'type' => self::TYPE_FLOAT 
				),
				// 地址列表json
				'ip_list' => array (
						'type' => self::TYPE_STRING,
						'size' => 1024 
				) 
		);
		parent::__construct ( $tableName, $size );
	}
}