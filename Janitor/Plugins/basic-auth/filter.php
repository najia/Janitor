<?php

define('REALM', 'Basic realm="'._JANITOR._VERSION."\"");

class BasicAuthFilter extends JanitorFilterInterface{
	
	public function __construct(){
		$this->config = array_merge($this->config,array(
				'name'=>'Basic Auth鉴权插件',
		));
	}
	
	public function getFilterName(){
		return __CLASS__;
	}
	
	public function getFilterType(){
		return 'pre';
	}
	
	public function getPriority(){
		return 100;
	}
	
	public function shouldFilter(JanitorHttpContext $ctx){
		return false;
	}
	
	public function run(JanitorHttpContext $ctx){
		$Authorization = $ctx->extractor->header('Authorization');
		$proxyAuthorization = $ctx->extractor->header('proxy-authorization');
		if (empty($Authorization) && empty($proxyAuthorization)){
			$ctx->response->status(401);
			$ctx->addJanitorHeader('WWW-Authenticate', REALM);
			$ctx->responseBody = '401';
			return false;
		}else{
			list($username,$password) = $this->getCredentials($ctx, 'proxy-authorization');
			$credentials = $this->loadCredentialsFromCache($ctx, $username);
			if (!$credentials){
				list($username,$password) = $this->getCredentials($ctx, 'authorization');
				$credentials = $this->loadCredentialsFromCache($ctx, $username);
			}
			if (!$credentials){
				$ctx->response->status(403);
			}
			$digest = sha1($credentials['uid'].$password);
			if ($digest!=$password){
				$ctx->response->status(403);
			}
			return true;
		}
	}
	
	protected function loadCredentialsFromCache(JanitorHttpContext $ctx,$username){
		$userCache = $ctx->janitorCtx->userDict;
		$credential = $userCache->select()->where('uid', $username)->commit();
		return $credential;
	}
	
	protected function getCredentials(JanitorHttpContext $ctx,$headerName){
		$authorizationHeader = $ctx->extractor->header($headerName);
		$username = '';
		$password = '';
		$authorizationHeader = trim($authorizationHeader);
		if (strlen($authorizationHeader)<6){
			return array($username,$password);
		}else{
			$decodeHeader = base64_decode($authorizationHeader);
			if ($decodeHeader){
				list($username,$password) = explode(':', $decodeHeader);
			}
		}
		return array($username,$password);
	}
}