<?php
interface JanitorPluginsInterface{
	
	public function config(JanitorContext $ctx,JanitorAdminHttpRequest $httpRequest);
}