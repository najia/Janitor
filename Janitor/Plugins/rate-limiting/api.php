<?php

class RateLimiting implements JanitorPluginsInterface{
	
	public function config(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$action = $request->get('action');
		switch ($action){
			case 'add':
				return $this->add($ctx, $request);
				break;
			case 'delete':
				return $this->delete($ctx, $request);
				break;
			case 'list':
				return $this->getlist($ctx,$request);
				break;
		}
		
	}
	
	
	protected function getlist(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		return $ctx->ipDict->select()->fetchAll();
	}
	
	protected function add(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$cache = $ctx->getCache(RateLimitingConfigCache::getName());
		$insertor = $cache->insert();
		$limitBy = $request->get('limit_by','ip');
		$insertor->set('key', RateLimitingConfigCache::getName());
		if ($limitBy=='consumer'||$limitBy=='ip'){
			$insertor->set('limit_by', $limitBy);
		}
		$policy = $request->get('policy','local');
		if ($policy=='local'||$policy=='cluster'){
			$insertor->set('policy', $policy);
		}
		$insertor->commit();
		return $cache->select()->fetchAll();
	}
	
	protected function delete(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		$cache = $ctx->getCache(basename(__DIR__), 'bucket');
		return $cache->select()->fetchAll();
	}
}