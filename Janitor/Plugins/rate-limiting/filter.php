<?php


class RateLimitingFilter extends JanitorFilterInterface{
	
	public function __construct(){
	}
	
	public function getFilterName(){
		return __CLASS__;
	}
	
	public function getFilterType(){
		return 'pre';
	}
	
	public function getPriority(){
		return 100;
	}
	
	public function shouldFilter(JanitorHttpContext $ctx){
		return true;
	}
	
	public function run(JanitorHttpContext $ctx){
		$config = $ctx->janitorCtx->getCache(RateLimitingConfigCache::getName());
		$identifier = $this->getIdentifier($ctx, $config);
		
		list($limit,$stop) = $this->getUsage($ctx, $config, $identifier, time());
		LOG_DEBUG("RateLimiting>>>%s",json_encode($limit));
		if (!$stop){
			
		}else{
			$ctx->response->status(429);
			$ctx->responseBody = "API rate limit exceeded";
		}
		$this->decrLimit($ctx, $config, $identifier, time());
		$this->addResponseHeader($ctx, $limit);
		return true;
	}
	
	protected function addResponseHeader(JanitorHttpContext $ctx,$limit){
		$name = array(
				'second_limit'=>'Second',
				'minute_limit'=>'Minute',
				'hour_limit'=>'Hour',
				'day_limit'=>'Day',
				'month_limit'=>'Month',
				'year_limit'=>'Year',
		);
		
		foreach ($limit as $k=>$v){
			
			if (isset($name[$k])){
				
				$ctx->addJanitorHeader('RATELIMIT_REMAINING_'.$name[$k], $v);
			}
		}
	}
	
	protected function decrLimit(JanitorHttpContext $ctx,RateLimitingConfigCache $config,$identifier,$curTimestamp){
		$bucketCache = $ctx->janitorCtx->getCache(RateLimitingBucketCache::getName());
		$limit = array(
			'second_limit',
			'minute_limit',
			'hour_limit',
			'day_limit',
			'month_limit',
			'year_limit',
		);
		foreach ($limit as $name){
			$bucketCache->decr()->where('key', md5($identifier))->at($name)->commit();
		}
	}
	
	
	protected function getUsage(JanitorHttpContext $ctx,RateLimitingConfigCache $config,$identifier,$curTimestamp){
		$bucketCache = $ctx->janitorCtx->getCache(RateLimitingBucketCache::getName());
		//查找该用户的限量信息
		$userLimit = $bucketCache->select()->findById(md5($identifier));
		if (!$userLimit){
			//初始化限量信息
			LOG_DEBUG("can not found identifier for key=%s",md5($identifier));
			$userLimit = $this->initDefaultLimit($ctx->janitorCtx, $identifier);
		}
		
		if (empty($userLimit)){
			$ctx->response->status(500);
			$ctx->responseBody = '500 internal error';
		}
		$insertor = $bucketCache->update();
		$lastFillTime = $userLimit['last_fill_time'];
		$ret = array($userLimit,false);
		LOG_DEBUG("%s>>curTime=%d,lastTime=%d",__METHOD__,$curTimestamp,$lastFillTime);
		foreach ($userLimit as $name=>$limit){
			if ($name=='second_limit'){
				if ($limit<=0){
					$ret = array($userLimit,$name);
					if ($lastFillTime<$curTimestamp){
						$insertor->set('second_limit', 10);//@todo user limit
					}
				}
			}
			if ($name=='minute_limit'){
				if ($limit<=0){
					$ret = array($userLimit,$name);
					if ($lastFillTime+60<$curTimestamp){
						$insertor->set('minute_limit', 10);//@todo user limit
					}
				}
			}
			if ($name=='hour_limit'){
				if ($limit<=0){
					$ret = array($userLimit,$name);
					if ($lastFillTime+3600<$curTimestamp){
						$insertor->set('hour_limit', 5);//@todo user limit
					}
				}
			}
			if ($name=='day_limit'){
				if ($limit<=0){
					$ret = array($userLimit,$name);
					if ($lastFillTime+3600*24<$curTimestamp){
						$insertor->set('day_limit', 5);//@todo user limit
					}
				}
			}
			if ($name=='month_limit'){
				if ($limit<=0){
					$ret = array($userLimit,$name);
					if ($lastFillTime+3600*24*30<$curTimestamp){
						$insertor->set('month_limit', 5);//@todo user limit
					}
				}
			}
			if ($name=='year_limit'){
				if ($limit<=0){
					$ret = array($userLimit,$name);
					if ($lastFillTime+3600*24*365<$curTimestamp){
						$insertor->set('year_limit', 5);//@todo user limit
					}
				}
			}
		}
		$insertor->set('last_fill_time', time())->where('key', md5($identifier))->commit();
		return $ret;
	}
	
	protected function initDefaultLimit(JanitorContext $ctx,$identifier){
		$bucketCache = $ctx->getCache(RateLimitingBucketCache::getName());
		$configCache = $ctx->getCache(RateLimitingConfigCache::getName());
	
		//@todo 默认的参数设置
		$bucketCache->insert()
					->set('key', md5($identifier))
					->set('second_limit', 5)
					->set('minute_limit', 50)
					->set('hour_limit', 50)
					->set('day_limit', 500)
					->set('month_limit', 500)
					->set('year_limit', 5000)
					->set('last_fill_time', time())
					->commit();
		return $bucketCache->select()->findById(md5($identifier));
	}
	
	protected function getIdentifier(JanitorHttpContext $ctx,RateLimitingConfigCache $config){
		$cacheConfig = $config->select()->findById(RateLimitingConfigCache::getName());
		$limitBy = $cacheConfig['limit_by'];
		if ($limitBy=='ip'){
			return $ctx->extractor->header('remote_addr');
		}else {
			if (isset($ctx->userCtx["uid"])){
				return $ctx->userCtx["uid"];
			}else{
				return $ctx->extractor->header('remote_addr');
			}
		}
	}
	
	
}