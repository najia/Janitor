<?php
/**
 * 储存ratelimiting的配置信息
 * @author Administrator
 *
 */
class RateLimitingConfigCache extends JanitorCacheBase{
	
	public function __construct($tableName, $size) {
		$this->definition = array (
				'key' => array (
						'primary' => true,
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				//consumer|ip
				'limit_by'=>array (
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				//local|cluster
				'policy'=>array (
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'redis_host'=>array (
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'redis_port'=>array (
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'redis_password'=>array (
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				
		);
		parent::__construct($tableName, $size);
	}
}