<?php
/**
 * 储存所有的bucket
 * @author Administrator
 *
 */
class RateLimitingBucketCache extends JanitorCacheBase{
	
	public function __construct($tableName, $size) {
		$this->definition = array (
				'key' => array (
						'primary' => true,
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'second_limit' => array (
						'type' => self::TYPE_FLOAT,
				),
				'minute_limit' => array (
						'type' => self::TYPE_FLOAT,
				),
				'hour_limit' => array (
						'type' => self::TYPE_FLOAT,
				),
				'day_limit' => array (
						'type' => self::TYPE_FLOAT,
				),
				'month_limit' => array (
						'type' => self::TYPE_FLOAT,
				),
				'year_limit' => array (
						'type' => self::TYPE_FLOAT,
				),
				'last_fill_time'=>array (
						'type' => self::TYPE_FLOAT,
				),
		);
		parent::__construct($tableName, $size);
	}
}