<?php

class DebugInfo implements JanitorPluginsInterface{
	
	public function config(JanitorContext $ctx,JanitorAdminHttpRequest $request){
		LOG_DEBUG("got config data=".$request->request('ip'));
		$ipcidr  = $request->request('ip');
		JanitorEventHandler::getInstance()->invoke(IP_RESTRICT_EVENT, $ipcidr);
		return $ctx->ipDict->select()->fetchAll();
	}
}