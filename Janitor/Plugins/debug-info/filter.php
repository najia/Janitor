<?php

define('IP_RESTRICT_EVENT', 'onAddIPCidr');

class DebugInfoFilter extends JanitorFilterInterface {
	
	protected $isLoadCidir;
	
	public function __construct(){
		$this->isLoadCidir = false;
	}
	
	public function getFilterName(){
		return __CLASS__;
	}
	
	public function getFilterType(){
		return 'pre';
	}
	
	public function getPriority(){
		return 100;
	}
	
	public function shouldFilter(JanitorHttpContext $ctx){
		return true;
	}
	
	public function run(JanitorHttpContext $ctx){
		$ctx->addJanitorHeader('X-DEBUG-SERVER', json_encode($ctx->request->server));
		$ctx->addJanitorHeader('X-DEBUG-HEAD', json_encode($ctx->request->header));
	}
}