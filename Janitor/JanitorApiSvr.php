<?php

class JanitorApiSvr implements JanitorService{

	protected $apiSvrConf;
	
	protected $janitorConf;
	
	protected $implSvr;
	
	public function start(JanitorContext $context){
		$this->janitorConf = $context->janitorConf;
		$this->apiSvrConf = $context->janitorConf->getAPIGatewayConf();
		
		$eventloop = new swoole_http_server($this->apiSvrConf->webSvrHost,$this->apiSvrConf->webSvrPort);
		
		$config = array(
				'enable_static_handler'=>$this->apiSvrConf->enableStaticHandler,
				'document_root' => dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR,
				'http_parse_post'=>$this->apiSvrConf->parsePost,
				'daemonize'=>true,
				'log_file'=>$this->apiSvrConf->logFile,
				'pid_file'=>$this->apiSvrConf->pidFile,
		);
		$eventloop->set($config);
		LOG_INFO("event loop config=>%s",json_encode($config));
		$this->implSvr = new JanitorApiImplsvr($context);
		
		$eventloop->on('request',array($this->implSvr,'onRequest'));
		$eventloop->on('workerStart', array($this->implSvr,'onWorkerStart'));
		
		$this->janitorConf->setEventloop($eventloop);
	}
		
	public function stop(JanitorContext $context){
	}
}