<?php
class JanitorFilterManager {
	const FILTER_TYPE_PRE = 'pre';
	const FILTER_TYPE_ROUTE = 'route';
	const FILTER_TYPE_POST = 'post';
	public $filters;
	public $filtersNum = array ();

	protected $ctx;

	public function __construct(JanitorContext $ctx){
		$this->ctx = $ctx;
	}

	public static function getInstance(JanitorContext $ctx) {
		static $m = null;
		if ($m == null) {
			$m = new JanitorFilterManager ($ctx);
		}
		return $m;
	}
	
	/**
	 *
	 * @param unknown $directorys        	
	 */
	public function init($directorys) {
		foreach ( $directorys as $directory ) {
			$dirs = $this->getFilterFiles ( $directory );
			//初始化所有的cache
			foreach ($dirs as $pluginName){
				$cacheDir = $directory. DIRECTORY_SEPARATOR.$pluginName . DIRECTORY_SEPARATOR .'cache';
				if (is_dir($cacheDir)){
					$cacheFileList = $this->getCacheFiles($cacheDir);
					if ($cacheFileList){
						$segments = explode ( '-', $pluginName );
						$className = '';
						foreach ( $segments as $segment ) {
							$className .= ucfirst ( $segment );
						}
						foreach ($cacheFileList as $cacheFile){
							require_once $cacheDir.DIRECTORY_SEPARATOR.$cacheFile;
							$cacheClassName = $className.ucfirst(substr($cacheFile, 0,strpos($cacheFile,'.'))).'Cache';
							try {
								$reflection = new ReflectionClass ( $cacheClassName);
								$instance = $reflection->newInstanceArgs(array($cacheClassName,1024*10));//@todo,cache的大小设定
								$this->ctx->pluginCaches[$cacheClassName] = $instance;
							} catch ( Exception $e ) {
								throw new JanitorException ( $e->getMessage(), 404 );
							}
						}
					}
				}
			}
			foreach ( $dirs as $pluginName ) {
				$filterFileName = $directory . DIRECTORY_SEPARATOR . $pluginName . DIRECTORY_SEPARATOR . 'filter.php';
				if (is_file ( $filterFileName )) {
					require_once $filterFileName;
					$segments = explode ( '-', $pluginName );
					$className = '';
					foreach ( $segments as $segment ) {
						$className .= ucfirst ( $segment );
					}
					$className .= 'Filter';
					try {
						$reflection = new ReflectionClass ( $className );
						$instance = $reflection->newInstance ();
						if ($instance instanceof JanitorFilterInterface) {
							$this->filters [$filterFileName] = $instance;
						}
					} catch ( Exception $e ) {
						throw new JanitorException ( "can not found filter for {$pluginName}", 404 );
					}
				} else {
					throw new JanitorException ( "plugin {$pluginName} expected a filter.php file entry" );
				}
			}
		}
		usort ( $this->filters, function ($f1, $f2) {
			if ($f1 instanceof JanitorFilterInterface && $f2 instanceof JanitorFilterInterface) {
				$priority1 = $f1->getPriority ();
				$priority2 = $f2->getPriority ();
				if ($priority1 == $priority2)
					return 0;
				else
					return $priority1 - $priority2 > 0 ? 1 : - 1;
			}
		} );
	}
	public function getFilters() {
		$ret = array ();
		foreach ( $this->filters as $filter ) {
			if ($filter instanceof JanitorFilterInterface) {
				$ret [$filter->getFilterName ()] = array (
						'type' => $filter->getFilterType (),
						'name' => $filter->config ['name'] 
				);
			}
		}
		LOG_DEBUG ( json_encode ( $ret ) );
		return $ret;
	}
	protected function getFilterFiles($directory) {
		$files = scandir ( $directory );
		foreach ( $files as $key => $file ) {
			if (! is_dir ( $directory . DIRECTORY_SEPARATOR . $file )) {
				unset ( $files [$key] );
			}
			if ($file == '.' || $file == '..') {
				unset ( $files [$key] );
			}
		}
		return $files;
	}
	
	protected function getCacheFiles($directory){
		$files = scandir ( $directory );
		foreach ( $files as $key => $file ) {
			if ( is_dir ( $directory . DIRECTORY_SEPARATOR . $file )) {
				unset ( $files [$key] );
			}
			if ($file == '.' || $file == '..') {
				unset ( $files [$key] );
			}
		}
		return $files;
	}
	public function runFilter(JanitorHttpContext $ctx) {
		foreach ( $this->filters as $filter ) {
			if ($filter instanceof JanitorFilterInterface) {
				LOG_DEBUG ( "run filter>>%s", $filter->getFilterName () );
				if ($filter->shouldFilter($ctx)){
					$filter->run ( $ctx );
				}
				
			}
		}
	}
}