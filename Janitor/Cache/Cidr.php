<?php
class JanitorCacheCidr extends JanitorCacheBase {
	
	public function __construct($tableName, $size) {
		$this->definition = array (
				'ipcidr' => array (
						'primary' => true,
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'lower_ip' => array (
						'type' => self::TYPE_FLOAT,
				),
				'upper_ip' => array (
						'type' => self::TYPE_FLOAT,
				),
		);
		parent::__construct($tableName, $size);
	}
}