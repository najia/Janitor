<?php
class JanitorCacheCustomer extends JanitorCacheBase {
	
	public function __construct($tableName, $size) {
		$this->definition = array (
				'uid' => array (
						'primary' => true,
						'type' => self::TYPE_STRING,
						'size' => 64 
				),
				'name' => array (
						'type' => self::TYPE_STRING,
						'size' => 64 
				),
				'password' => array (
						'type' => self::TYPE_STRING,
						'size' => 64
				),
				'description' => array (
						'type' => self::TYPE_STRING,
						'size' => 256 
				),
				'add_time' => array (
						'type' => self::TYPE_FLOAT 
				),
				'status' => array (
						'type' => self::TYPE_INT 
				) 
		);
		parent::__construct($tableName, $size);
	}
}