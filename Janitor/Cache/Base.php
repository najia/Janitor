<?php
abstract class Statement{
	
	protected $kvSet;
	
	protected $whereK;
	
	protected $whereV;
	
	protected $table;
	
	protected $tableName;
	
	protected $primaryKey;
	
	public function __construct(JanitorCacheBase $cache){
		$this->table = $cache->table;
		$this->tableName = $cache->tableName;
		$this->primaryKey = $cache->primaryName;
	}
	
	public function set($k,$v){
		$this->kvSet[$k] = $v;
		return $this;
	}
	
	public function where($k,$v){
		$this->whereK = $k;
		$this->whereV = $v;
		return $this;
	}
	
	abstract public function commit();
}
class Selector extends Statement{
	
	public function commit(){
		LOG_DEBUG("query row=%s",$this->whereV);
		return $this->table->get($this->whereV);
	}
	
	public function findById($key){
		$val = $this->table->get($key);
		LOG_DEBUG("FIND_BY_ID=%s,ret=%s",$key,json_encode($val));
		return $val;
	}
	
	public function fetchAll(){
		$data = array();
		foreach ($this->table as $k=>$row){
			$data[$k] = $row;
		}
		return $data;
	}
}

class Updater extends Statement{
	
	public function commit(){
		foreach ($this->kvSet as $k=>$v){
			if (strcmp($this->primaryKey, $k)==0){
				$this->whereV = $v;
				break;
			}
		}
		if (empty($this->whereV)){
			throw new JanitorException("primary key is missing");
		}
		LOG_DEBUG("UPDATE[%s]>>%s=%s,%s",$this->tableName,$this->whereK,$this->whereV,json_encode($this->kvSet));
		$this->table->set($this->whereV, $this->kvSet);
	}
}
// incr()->set('column',3)->commit();
class Incr extends Statement{
	
	protected $column;
	
	public function at($column){
		$this->column = $column;
		return $this;
	}
	
	public function commit(){
		$status = $this->table->incr($this->whereV, $this->column);
		LOG_DEBUG("INCR[%s]>>%s=%s,%s,status=%d",$this->tableName,$this->whereK,$this->whereV,$this->column,$status);
	}
}

class Decr extends Statement{
	
	protected $column;
	
	public function at($column){
		$this->column = $column;
		return $this;
	}
	
	public function commit(){
		$status = $this->table->decr($this->whereV, $this->column,1);
		LOG_DEBUG("DECR[%s]>>%s=%s,%s,status=%d",$this->tableName,$this->whereK,$this->whereV,$this->column,$status);
	}
}

class Delete extends Statement{
	
	public function commit(){
		LOG_DEBUG("DELETE[%s]>>%s=%s,%s",$this->tableName,$this->whereK,$this->whereV,json_encode($this->kvSet));
		$this->table->del($this->whereV);
	}
}

class Insertor extends Statement{
	
	public function commit(){
		foreach ($this->kvSet as $k=>$v){
			if (strcmp($this->primaryKey, $k)==0){
				$this->whereV = $v;
				break;
			}
		}
		if (empty($this->whereV)){
			throw new JanitorException("primary key is missing");
		}
		LOG_DEBUG("INSERT[%s]>>%s=%s,%s",$this->tableName,$this->primaryKey,$this->whereV,json_encode($this->kvSet));
		$this->table->set($this->whereV, $this->kvSet);
	}
}

class JanitorCacheBase{
	
	const TYPE_INT = 'INT';
	const TYPE_FLOAT = 'FLOAT';
	const TYPE_STRING = 'STRING';
	
	public $table;
	
	protected $definition = array();//array('col1'=>is|not primary,'col2'=>)
	
	public $primaryName;
	
	public $tableName;
	
	protected $dumpFile;
	
	protected $size;
	
	public static function getName(){
		return get_called_class();
	}
	
	public function __construct($tableName,$size){
		if (empty($this->definition)){
			throw new JanitorException("empty definition is not allowed");
		}
		$this->tableName = $tableName;
		$dumpDir = JanitorConfiguration::getProperty('janitor.dump.dir',dirname(dirname(__FILE__)));
		if (!is_dir($dumpDir)){
			mkdir($dumpDir);
		}
		
		$this->dumpFile = $dumpDir.DIRECTORY_SEPARATOR.$this->tableName.'.dump.json';
		
		$this->table = new swoole_table($size);
		
		foreach ($this->definition as $name=>$def){
			if (isset($def['primary']) && $def['primary']){
				$this->primaryName = $name;
			}
			if (!isset($def['type'])){
				throw new JanitorException("cache field for {$name},type is missing");
			}
			switch ($def['type']){
				case self::TYPE_FLOAT:
					LOG_DEBUG("CREATE TABLE:%s>>>>>columnName=%s type=float",$this->tableName,$name);
					$this->table->column($name, swoole_table::TYPE_FLOAT);
					break;
				case self::TYPE_INT:
					LOG_DEBUG("CREATE TABLE:%s>>>>>columnName=%s type=int",$this->tableName,$name);
					$this->table->column($name, swoole_table::TYPE_INT);
					break;
				case self::TYPE_STRING:
					if (!isset($def['size'])){
						throw new JanitorException("can not define a string column[{$name}] withou size");
					}
					LOG_DEBUG("CREATE TABLE:%s>>>>>columnName=%s(%d) type=string",$this->tableName,$name,$def['size']);
					$this->table->column($name, swoole_table::TYPE_STRING,intval($def['size']));
					break;
			}
		}
		$this->table->create();
		$this->trySync();
	}
	
	protected function trySync(){
		try {
			if (is_file($this->dumpFile)){
				$content = file_get_contents($this->dumpFile);
				$data = json_decode($content,true);
				if ($data){
					foreach ($data as $key=>$v){
						$this->table->set($key, $v);
// 						$type = $this->definition[$k]['type'];
// 						if ($type==self::TYPE_FLOAT){
// 							$this->table->set($k, floatval($v));
// 						}
// 						if ($type==self::TYPE_INT){
// 							$this->table->set($key, intval($v));
// 						}
// 						if ($type==self::TYPE_STRING){
// 							$this->table->set($key, $v);
// 						}
						
					}
				}
				LOG_DEBUG("trySync %d record from file %s",count($data),$this->dumpFile);
			}else{
				LOG_DEBUG("trySync abort file %s is not exits",$this->dumpFile);
			}
		} catch (Exception $e) {
			LOG_DEBUG("trySync from %s failed,%s",$this->dumpFile,$e->getMessage());
		}
	}
	
	public function __destruct(){
		$data = array();
		foreach ($this->table as $k=>$value) {
			$data[$k] = $value;
		}
		file_put_contents($this->dumpFile, json_encode($data));
		LOG_DEBUG("SYNC[%s]>>%s to file",$this->dumpFile,json_encode($data));
	}
	
	public function update(){
		return new Updater($this);
	}
	
	public function insert(){
		return new Insertor($this);
	}
	
	public function delete(){
		return new Delete($this);
	}
	
	public function incr(){
		return new Incr($this);
	}
	
	public function decr(){
		return new Decr($this);
	}
	
	public function select(){
		return new Selector($this);
	}
}