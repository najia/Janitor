<?php

class JanitorRpcImplsvr{
	
	/**
	 * 
	 * @var JanitorContext
	 */
	protected $ctx;
	
	public function __construct(JanitorContext $context){
		$this->ctx = $context;
	}
	
	public function onConnect(swoole_server $server, $fd, $from_id){
		var_dump("got connection from ".$from_id);
	}
	
	public function onClose(swoole_server $server, $fd, $reactorId){
		
	}
	
	public function onPacket(swoole_server $server, $data, $addr){
		$server->sendto($addr['address'],$addr['port'],'Swoole: '.$data."\n"."the current reqNum is:".$this->ctx->requestNums->get()."\n");
	}
}