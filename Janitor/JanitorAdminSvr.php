<?php

class JanitorAdminSvr implements JanitorService{
	
	protected $adminSvrConf;
	
	protected $janitorConf;
	
	protected $implSvr;

	public function start(JanitorContext $context){
		$this->adminSvrConf = $context->janitorConf->getAdminSvrConf();
		$this->janitorConf = $context->janitorConf;
		
		$eventloop = $this->janitorConf->getEventloop();
		
		if ($eventloop){
			
			$enableSQLPool = JanitorConfiguration::getBoolean("janitor.enable_sql_pool",false);
			LOG_DEBUG("is enable sql pool=%s",$enableSQLPool);
			if ($enableSQLPool){
				$sqlPool = new JanitorAdminSqlpool($context);
				$context->sqlPool = $sqlPool;
			}
			
			$adminSvr = $eventloop->addlistener($this->adminSvrConf->statSvrHost, $this->adminSvrConf->statSvrPort, SWOOLE_SOCK_TCP);
			
			$this->implSvr = new JanitorAdminImplsvr($context);
			$adminSvr->on('request',array($this->implSvr,'onRequest'));
		}else{
			throw new Exception("can not init stat svr before api svr is inited...");
		}
		
	}
	
	public function stop(JanitorContext $context){
		foreach ($context->userDict as $k=>$v){
			LOG_DEBUG(">>>>>%s=%s",$k,json_encode($v));
		}
	}
}