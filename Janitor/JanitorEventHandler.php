<?php

class JanitorEventHandler{
	
	protected $eventMap;
	
	/**
	 * 
	 * @var JanitorContext
	 */
	protected $ctx;
	
	public static function getInstance(){
		static $m = null;
		if ($m==null) {
			$m = new JanitorEventHandler();
		}
		return $m;
	}
	
	public function init(JanitorContext $ctx){
		$this->ctx = $ctx;
	}
	
	public function addListener($what,JanitorEventListener $listener){
		$this->eventMap[$what] = $listener;
	}
	
	public function invoke($what,$data){
		foreach ($this->eventMap as $_what=>$listener){
			if ($what==$_what && ($listener instanceof JanitorEventListener)){
				$evt = new JanitorEvent($what,$data);
				$listener->onInvoke($this->ctx, $evt);
			}
		}
	}
}