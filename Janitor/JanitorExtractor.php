<?php
class JanitorExtractor{
	
	const TYPE_INDEX = 1;//通过index索引
	const TYPE_TPL = 2;//通过名称索引
	
	
	const EXTRACTOR_HEADER = 'Header';
	const EXTRACTOR_QUERY = 'Query';
	const EXTRACTOR_POST = 'Post';
	const EXTRACTOR_HOST = 'Host';
	const EXTRACTOR_IP = 'IP';
	const EXTRACTOR_METHOD = 'Method';
	const EXTRACTOR_COOKIE = 'Cookie';
	
	protected $httpRequest;
	
	public function __construct(swoole_http_request $request){
		$this->httpRequest = $request;
	}
	
	public function header($k,$v=null){
		if (isset($this->httpRequest->header[$k])){
			LOG_DEBUG("try to extract header key=%s,value=%s",$k,$v);
			return $this->httpRequest->header[$k];
		}
		return $v;
	}
	
	public function remoteIP(){
		$host = $this->header("Host");
		list($ip,$port) = explode(':', $host);
		return $ip;
	}
	
	public function server($k,$v=null){
		if (isset($this->httpRequest->server[$k])){
			LOG_DEBUG("try to extract server key=%s,value=%s",$k,$v);
			return $this->httpRequest->server[$k];
		}
		return $v;
	}
	
	public function cookie($k,$v=null){
		if (isset($this->httpRequest->cookie[$k])){
			LOG_DEBUG("try to extract cookie key=%s,value=%s",$k,$v);
			return $this->httpRequest->cookie[$k];
		}
		return $v;
	}
	
	public function query($k,$v=null){
		if (isset($this->httpRequest->get[$k])){
			LOG_DEBUG("try to extract querystring key=%s,value=%s",$k,$v);
			return $this->httpRequest->get[$k];
		}
		return $v;
	}
	
	public function uri($pattern,$v=null){
		
	}
}