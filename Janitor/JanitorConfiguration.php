<?php

class JanitorConfiguration{
	
	protected static $map;
	/**
	 * 初始化配置文件
	 * @param unknown $filepath
	 */
	public static function init(){
		$options = getopt('c::');
		if (!isset($options['c'])){
			throw new JanitorException("can not init janitor due to lack of configuration file");
		}
		$filepath = $options["c"];
		if (!is_file($filepath)){
			throw new JanitorException("{$filepath} is not exists");
		}
		self::$map = parse_ini_file($filepath,true);
	}
	
	
	public static function getProperty($key,$default = null){
		if (empty($key))return $default;
		return isset(self::$map[$key])?self::$map[$key]:$default;
	}
	
	public static function getBoolean($key,$default = false){
		$value = self::getProperty($key,$default);
		return boolval($value);
	}
	
	public static function getInt($key,$default = 0){
		$value = self::getProperty($key,$default);
		return intval($value);
	}
	
}