<?php

class JanitorLBSvr implements JanitorService{

	protected $lbSvrConf;
	
	protected $janitorConf;
	
	protected $implSvr;
	
	public function start(JanitorContext $context){
		$this->lbSvrConf = $context->janitorConf->getLBSvrConf();
		$this->janitorConf = $context->janitorConf;
		
		$eventloop = $this->janitorConf->getEventloop();
		
		if ($eventloop){
			$lbSvr = $eventloop->addlistener($this->lbSvrConf->lbSvrHost, $this->lbSvrConf->lbSvrPort, SWOOLE_SOCK_TCP);
			$lbSvr->set(array(
					'daemonize'=>true,
					'log_file'=>$this->lbSvrConf->logFile,
					'pid_file'=>$this->lbSvrConf->pidFile,
			));
			$this->implSvr = new JanitorLbImplsvr($context);
			
			$lbSvr->on('connect',array($this->implSvr,'onConnect'));
			$lbSvr->on('close',array($this->implSvr,'onClose'));
			$lbSvr->on('receive',array($this->implSvr,'onReceive'));
			
		}else{
			throw new Exception("can not init stat svr before api svr is inited...");
		}
	}

	public function stop(JanitorContext $context){
		var_dump('stop lb svr');
	}
}