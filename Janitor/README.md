# Janitor
Janitor是一个基于swoole生态的PHP高性能API网关
一个httpServer提供api gateway的功能
一个stat htttwebserver 提供统计功能
一个tcp server提供配置下发功能
一个udp server提供L5的功能

操作纬度
global：全局操作纬度
user：session用户操作纬度
api：某一个api模式下的操作纬度

plugins功能
1）Transform Request Header,自动追加header信息和删除header信息
2）URLRewrite 模块，就是uri转换，被转换的url可以用serviceID来代替
3）Request Method Transform
4）Global Cache操作，
	4.1）针对URI，statuscode 缓存
	4.2）针对path-info缓存
5）API级别的LoadBalance 模块，根据IP和Host LoadBalance
6）Circuit Breakers 请求降级
7）Force Timeout Plugin