<?php
class JanitorLogger {
	const LOG_DEBUG = 1;
	const LOG_INFO = 2;
	const LOG_WARN = 3;
	const LOG_FATAL = 4;
	protected static $logfile;
	protected static $levelStrMap = array (
			'LOG_DEBUG' => 'DEBUG',
			'LOG_INFO' => 'INFO',
			'LOG_WARN' => 'WARN',
			'LOG_FATAL' => 'FATAL'
	);
	public static function setLoggerFile($logfile) {
		self::$logfile = $logfile;
	}
	public function log() {
		$backtrace = debug_backtrace ();
		if (count ( $backtrace ) >= 2) {
			$caller = $backtrace [1];
		} else {
			return;
		}
		
		$file = $caller ['file'];
		$line = $caller ['line'];
		$loglevel = $caller ['function'];
		$extra = $caller ['args'];
		$msg = call_user_func_array ( 'sprintf', $extra );
		$logmsg = sprintf (
				"[%s *%d]\t%s\t%s:%d\t%s\n",
				date ( 'Y-m-d H:i:s' ),
				getmypid(),
				self::$levelStrMap [$loglevel],
				basename ( $file ),
				$line,
				$msg
				);
		echo $logmsg;
	}
}

/**
 * Undocumented function
 *
 * @param [type] $file
 * @param [type] $line
 * @param [type] $func
 * @param [type] $fmt
 * @return void
 */
function LOG_DEBUG($fmt, $args = null) {
	return;
	$arg = func_get_args ();
	call_user_func_array ( array (
			'JanitorLogger',
			'log'
	), $arg );
}
function LOG_INFO($fmt, $args = null) {
	$arg = func_get_args ();
	call_user_func_array ( array (
			'JanitorLogger',
			'log'
	), $arg );
}
function LOG_WARN($fmt, $args = null) {
	$arg = func_get_args ();
	call_user_func_array ( array (
			'JanitorLogger',
			'log'
	), $arg );
}
function LOG_FATAL($fmt, $args = null) {
	$arg = func_get_args ();
	call_user_func_array ( array (
			'JanitorLogger',
			'log'
	), $arg );
}
