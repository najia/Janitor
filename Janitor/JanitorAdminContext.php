<?php
class JanitorAdminContext{
	/**
	 *
	 * @var JanitorContext
	 */
	public $janitorCtx;
	/**
	 *
	 * @var swoole_http_request
	 */
	public $request;
	/**
	 *
	 * @var swoole_http_response
	 */
	public $response;
	
	public function __construct(swoole_http_request $request,swoole_http_response $response){
		$this->request = $request;
		$this->response = $response;
	}
}